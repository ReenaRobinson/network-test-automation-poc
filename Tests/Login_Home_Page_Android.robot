*** Settings ***
Documentation     Script to verify DHOTT application login successful
Suite Setup       Default Suite Setup
Suite Teardown    Default Suite Teardown
Resource          ../Global/vars.robot
Resource          ../Global/keywords.robot
Library           AppiumLibrary

*** Test Cases ***
Login Application
    [Documentation]    Script opens DHOTT application
    Login Application
    List Recently Watched Items
    Logout Application

*** Keywords ***
Default Suite Setup
    Open Application    ${REMOTE_URL}    platformName=${PLATFORM_NAME_ANDROID}    platformVersion=${PLATFORM_VERSION_ANDROID}    deviceName=${DEVICE_NAME_ANDROID}    appPackage=nl.tmobiletv.vinson    appActivity=com.huawei.ott.module.login.activity.LoginActivity

Default Suite Teardown
    Close All Applications
