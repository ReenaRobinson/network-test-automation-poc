*** Settings ***
Documentation     Script to verify DHOTT application login successful IOS device
Suite Setup       Default Suite Setup
Suite Teardown    Default Suite Teardown
Resource          ../Global/vars.robot
Resource          ../Global/keywords.robot
Library           AppiumLibrary

*** Test Cases ***
Login Application iOS
    [Documentation]    Script opens browser and launch www.ranorex.com
    Login Application IOS
    List Recently Watched Items IOS
    Logout Application IOS

*** Keywords ***
Default Suite Setup
    Open Application    ${REMOTE_URL}    platformName=${PLATFORM_NAME_IOS}    platformVersion=${PLATFORM_VERSION_IOS}    deviceName=${DEVICE_NAME_IOS}    udid=${UDID}

Default Suite Teardown
    Close All Applications
