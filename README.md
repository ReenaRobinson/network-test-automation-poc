# Network Test Automation POC

## Test Script

1. Voice calls: Script to test the voice calls ( dialing another destination number)
2. Data sessions: Script try to accessing another website


### Clone the test suite to your local setup
```
git clone https://ReenaRobinson@bitbucket.org/ReenaRobinson/network-test-automation-poc.git
```

You will then be able to keep it up-to-date with the standard Git procedure:
#### Update from git
Keep the repository up-to-date with the standard GIT procedure
```
git pull
```

### Command to execute test script
```
robot <testscript>

eg: robot Voice_Calls_Verification_Android.robot
```

### Successful suite execution will return result files:
* output.xml
* log.html
* report.html