*** Variables ***
#************************ Common Variables ****************************
${REMOTE_URL}     http://localhost:4723/wd/hub    # URL to appium server
${BROWSER_IOS}    Safari
${PLATFORM_NAME_IOS}    iOS
${UDID}           37298c48a07ddf03a56d5451aae70880a0c50ff3
${PLATFORM_VERSION_IOS}    iOS 12.1.4
${DEVICE_NAME_IOS}    reena’s iPhone
${BROWSER_ANDROID}    Chrome
${PLATFORM_NAME_ANDROID}    Android
${PLATFORM_VERSION_ANDROID}    8.0.0
${DEVICE_NAME_ANDROID}    T-mobile Tester
