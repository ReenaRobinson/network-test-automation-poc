*** Keywords ***
Login Application
    Click Element    xpath=//*[@id='login_id']
    Input Text    xpath=//*[@id='login_id']    9357268273
    Click Element    xpath=//*[@id='login_pwd']
    Input Text    xpath=//*[@id='login_pwd']    6494
    Click Element    xpath=//*[@id='login_btn']
    wait Until Page Contains Element    xpath=//*[@id='page_navigate_left_logo']    5s
    Wait until Page Contains Element    xpath=//*[@text='Movie, TV Shows, Actor, Director']    5s

Login Application IOS
    Click Element    xpath=//*[@text='DHOTT']
    Click Element    xpath=//*[@text='Clear text']
    Click Element    xpath=//*[@placeholder='TV-Account Number']
    Input Text    xpath=//*[@placeholder='TV-Account Number']    9357268273
    Click Element    xpath=//*[@placeholder='TV-Pincode']
    Input Text    xpath=//*[@placeholder='TV-Pincode']    6494
    Click Element    xpath=//*[@text='Login_AccountLogin_mLoginBtn']
    wait Until Page Contains Element    xpath=//*[@text='Home_logoView']    3s
    Wait until Page Contains Element    xpath=//*[@text='Home_SearchView']    3s

List Recently Watched Items
    Click Element    xpath=//*[@id='page_navigate_right_img']
    Click Element    xpath=//*[@id='page_navigate_left_img']

Logout Application
    Click Element    xpath=//*[@id='imageview_menubar_icon' and (./preceding-sibling::* | ./following-sibling::*)[@text='ACCOUNT']]
    Click Element    xpath=(//*[@id='fm_admin_profile']/*[@class='android.widget.ImageView'])[1]
    Click Element    xpath=//*[@text='Log Out']
    Click Element    xpath=//*[@text='Yes']

List Recently Watched Items IOS
    Click Element    xpath=//*[@text='history bar 24']
    Click Element    xpath=//*[@text='Public_PageNavigate_mleftBtn']
    Click Element    xpath=//*[@text='Cancel']

Logout Application IOS
    Click Element    xpath=//*[@text='Admin']
    Click Element    xpath=(//*[@text='Profile_IndexTableView']/*[@text='My_Profile_MLogoutButton'])[1]
    Click Element    xxpath=//*[@accessibilityLabel='Public_MessageDialogContentViewController_rightButton']
